#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Pose2D.h>
#include "tf/transform_datatypes.h"

using namespace std;
using namespace ros;

Publisher posePub;

void odomCB(const nav_msgs::Odometry &msg) {
    geometry_msgs::Pose2D pose;
    pose.x = msg.pose.pose.position.x;
    pose.y = msg.pose.pose.position.y;

    tf::Quaternion quat;
    tf::quaternionMsgToTF(msg.pose.pose.orientation, quat);
    // the tf::Quaternion has a method to acess roll pitch and yaw
    double roll, pitch, yaw;
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
    
    pose.theta = yaw;
    posePub.publish(pose);
}

int main(int argc, char **argv)
{
    init(argc, argv, "odomToPose2D");

    NodeHandle nh;

    Subscriber odomSub = nh.subscribe("/odom", 1, odomCB);
    posePub = nh.advertise<geometry_msgs::Pose2D>("/robot/position/pose2d", 1);

    spin();

    return 0;
}
