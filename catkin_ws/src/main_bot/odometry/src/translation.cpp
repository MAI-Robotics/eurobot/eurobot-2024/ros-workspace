#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include "std_msgs/Int16.h"
#include <cmath>

void leftposCallback(const std_msgs::Int16::ConstPtr& msg)
{
  int currentposleft = msg->data;                  //test to just recevive data
}

void rightposCallback(const std_msgs::Int16::ConstPtr& msg)
{
  int currentposright = msg->data;                  //test to just recevive data
}

struct RobotState
{
  double x;
  double y;
  double theta;
};


int main(int argc, char** argv){
  ros::init(argc, argv, "odometry_publisher");

  ros::NodeHandle n;
  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("odom", 50);
  tf::TransformBroadcaster odom_broadcaster;
  RobotState state;

  double currentPosright = static_cast<double>(currentPosright);
  double currentPosleft = static_cast<double>(currentPosleft);

  double x = 0.0;
  double y = 0.0;
  double th = 0.0;
  double distance_average;

  double vx = 0.1;
  double vy = -0.1;
  double vth = 0.1;

  ros::Time current_time, last_time;
  current_time = ros::Time::now();
  last_time = ros::Time::now();

  ros::Rate r(1.0);


  while(n.ok()){

    ros::spinOnce();               // check for incoming messages
    current_time = ros::Time::now();
    ros::Subscriber pub_posleft = n.subscribe("left_pos", 1000, leftposCallback);
    ros::Subscriber pub_posright = n.subscribe("right_pos", 1000,rightposCallback);
    
    state.theta = (currentPosright - currentPosleft) / 0.32;
    distance_average = (currentPosleft + currentPosright / 2.0);
    x += distance_average * cos(th + state.theta / 2.0);
    y += distance_average * sin(th + state.theta / 2.0);
    th += state.theta;

    //since all odometry is 6DOF we'll need a quaternion created from yaw
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    //first, we'll publish the transform over tf
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_footprint";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;

    //send the transform
    odom_broadcaster.sendTransform(odom_trans);

    //next, we'll publish the odometry message over ROS
    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    odom.header.frame_id = "odom";

    //set the position
    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    //set the velocity
    odom.child_frame_id = "base_footprint";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    //publish the message
    odom_pub.publish(odom);

    last_time = current_time;
    r.sleep();
  }
}