#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include <iostream>
#include <chrono>
#include <thread>

class PIDController {
private:
    double Kp, Ki, Kd;
    double setpoint, integral, lastError;

public:
    PIDController(double p, double i, double d) : Kp(p), Ki(i), Kd(d), setpoint(0.0), integral(0.0), lastError(0.0) {}

    double calculate(double actualValue) {
        double error = setpoint - actualValue;
        integral += error;
        double derivative = error - lastError;

        double output = Kp * error + Ki * integral + Kd * derivative;

        lastError = error;
        return output;
    }

    void setSetpoint(double sp) {
        setpoint = sp;
    }
};

class MotorController {
private:
    PIDController linearPID, angularPID;
    double encoderFeedbackLeft, encoderFeedbackRight;
    double pwmMin, pwmMax;


public:
    MotorController(double linearP, double linearI, double linearD, double angularP, double angularI, double angularD, double minPWM, double maxPWM)
        : linearPID(linearP, linearI, linearD), angularPID(angularP, angularI, angularD), pwmMin(minPWM), pwmMax(maxPWM) {}


    void update(double linearSpeed, double angularSpeed) {
        // Assuming linear and angular speeds are in m/s
        double leftSpeed = (linearSpeed - angularSpeed / 2.0) * 100.0;  // Convert to cm/s
        double rightSpeed = (linearSpeed + angularSpeed / 2.0) * 100.0;

        // Use encoder feedback to adjust speeds
        leftSpeed += linearPID.calculate(encoderFeedbackLeft);
        rightSpeed += linearPID.calculate(encoderFeedbackRight);

        // Ensure speeds are within PWM limits
        leftSpeed = std::min(std::max(leftSpeed, pwmMin), pwmMax);
        rightSpeed = std::min(std::max(rightSpeed, pwmMin), pwmMax);

        // Apply PWM signals to motors
        std::cout << "Left PWM: " << leftSpeed << ", Right PWM: " << rightSpeed << std::endl;

        // Send pwm to motors via rosserial
        //std::this_thread::sleep_for(std::chrono::milliseconds(100));

        // get encoder data
        encoderFeedbackLeft += leftSpeed / 100.0;  // Convert back to m/s
        encoderFeedbackRight += rightSpeed / 100.0;
    }
};

void leftposCallback(const std_msgs::Int16::ConstPtr& msg){
    double encoderFeedbackLeft = msg->data;    
}

void rightposCallback(const std_msgs::Int16::ConstPtr& msg)
{
    double encoderFeedbackRight = msg->data;                  
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "motor_handler");
    ros::NodeHandle nh;
    ros::Publisher left_pwm_publisher = nh.advertise<std_msgs::Int16>("Left_PWM", 10);
    ros::Publisher right_pwm_publisher = nh.advertise<std_msgs::Int16>("Right_PWM", 10);
    ros::Subscriber pub_posleft = nh.subcribe("left_pos", 1000, leftposCallback);
    ros::Subscriber pub_pubright = nh.subcribe("right_pos", 1000, rightposCallback)
    // Create a MotorController with PID parameters and PWM limits
    MotorController motorController(0.1, 0.01, 0.05, 0.1, 0.01, 0.05, -300.0, 300.0);

    // Set desired linear and angular speeds
    double linearSpeed = 0.1;  // m/s
    double angularSpeed = 0.2; // rad/s

    // Update the motor controller with desired speeds
    motorController.update(linearSpeed, angularSpeed);
    std_msgs::Int16 left_pwm_msgs;
    left_pwm_mssg.data = static_cast<int>(int left_speed);
    left_pwm_publisher.publish(left_pwm_msg);
    right_pwm_msgs.data = static_cast<int>(int right_speed);
    right_pwm_publisher.publish = (right_pwm_msg);

    return 0;
}
