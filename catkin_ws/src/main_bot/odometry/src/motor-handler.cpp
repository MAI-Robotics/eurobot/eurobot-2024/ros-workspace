#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Int16.h"

class WheelVelocitiesCalculator {
public:
    WheelVelocitiesCalculator(double wheel_radius, double wheel_base)
        : wheel_radius_(wheel_radius), wheel_base_(wheel_base) {}

    //Method for setting the publishers
    void setPublishers(ros::NodeHandle& nh) {
        left_pwm_pub_ = nh.advertise<std_msgs::Int16>("left_pwm", 10);
        right_pwm_pub_ = nh.advertise<std_msgs::Int16>("right_pwm", 10);
    }

    void calculateWheelVelocities(const geometry_msgs::Twist::ConstPtr& cmd_vel) {
        double linear_velocity = cmd_vel->linear.x;
        double angular_velocity = cmd_vel->angular.z;

        // Calculate wheel velocities
        double left_wheel_velocity = linear_velocity - (angular_velocity * wheel_base_ / 2.0);
        double right_wheel_velocity = linear_velocity + (angular_velocity * wheel_base_ / 2.0);

        // Convert wheel velocities to PWM values
        int16_t left_pwm = calculatePWM(left_wheel_velocity);
        int16_t right_pwm = calculatePWM(right_wheel_velocity);

        // Publish PWM values
        std_msgs::Int16 left_pwm_msg;
        left_pwm_msg.data = left_pwm;
        left_pwm_pub_.publish(left_pwm_msg);

        std_msgs::Int16 right_pwm_msg;
        right_pwm_msg.data = right_pwm;
        right_pwm_pub_.publish(right_pwm_msg);

        // Print or use the PWM values as needed
        ROS_INFO("Left PWM: %d, Right PWM: %d", left_pwm, right_pwm);
    }

private:
    double wheel_radius_;
    double wheel_base_;
    ros::Publisher left_pwm_pub_;
    ros::Publisher right_pwm_pub_;
    // Method for converting wheel rotation speed into PWM value
    int16_t calculatePWM(double wheel_velocity) {
<<<<<<< HEAD
        //double time_for_1_meter_at_50_pwm = 18.0;  // in Sekunden
        double max_pwm = 300.0;
        // Berechne PWM-Wert basierend auf der Raddrehgeschwindigkeit
=======
        //double time_for_1_meter_at_50_pwm = 18.0;  
        double max_pwm = 300.0;
        // Calculate PWM value based on wheel rotation speed
>>>>>>> 7dbadfea77bd7ffb01179be7070395a9baafb873
        int16_t pwm_value = static_cast<int16_t>((wheel_velocity/100));

        // Limit the PWM value to the maximum
        return std::min(pwm_value, static_cast<int16_t>(max_pwm));
    }
};

int main(int argc, char **argv) {
    ros::init(argc, argv, "wheel_velocities_calculator");
    ros::NodeHandle nh;

    WheelVelocitiesCalculator calculator(0.023, 0.32);  // 4.6 cm diameter converted to meters, 32 cm wheelbase converted to meters

    //Setting up Publishers
    calculator.setPublishers(nh);

    ros::Subscriber cmd_vel_sub = nh.subscribe("cmd_vel", 10, &WheelVelocitiesCalculator::calculateWheelVelocities, &calculator);

<<<<<<< HEAD
    ros::spin();  // Keep the node running 
=======
    ros::spin();  // Keep the node running

>>>>>>> 7dbadfea77bd7ffb01179be7070395a9baafb873
    return 0;
}
