# Changelog
## Week 2
- Started with the first workspace on ROS 2
- Switched to ROS 1 because of compatibility problems (rplidar, rosserial)
- first ros workspace on Raspberry Pi 4b

## Week 3
- Started with docker / vm with synced workspace
- Started with lidar, hector slam and gazebo

## Week 4
- Working on navstack
- Writing own package for unit conversion

## Week 5 + 6
- Finishing our package for Odometry and motor handler
- tf is setup
- Testing with roscore

## Week 7
- Coordinate system implemented
- Working on a Pid-Controller for the motors